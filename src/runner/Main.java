package runner;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Iterator;
import java.util.Locale;
import java.util.TreeSet;
import java.util.regex.Pattern;

import cc.mallet.pipe.CharSequence2TokenSequence;
import cc.mallet.pipe.CharSequenceLowercase;
import cc.mallet.pipe.Pipe;
import cc.mallet.pipe.SerialPipes;
import cc.mallet.pipe.TokenSequence2FeatureSequence;
import cc.mallet.pipe.iterator.CsvIterator;
import cc.mallet.topics.DMRLoader;
import cc.mallet.topics.DMRTopicModel;
import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.topics.TopicInferencer;
import cc.mallet.types.Alphabet;
import cc.mallet.types.FeatureSequence;
import cc.mallet.types.IDSorter;
import cc.mallet.types.Instance;
import cc.mallet.types.InstanceList;
import cc.mallet.types.LabelSequence;

public class Main {
	
	public static void main(String [] args) throws IOException
	{
		//DMR
//		DMRLoader loader = new DMRLoader();
//		
//		File docFile = new File("C:/Users/usx29096/Desktop/malletTest/paperAbstract"); //txt format 1 doc per line
//		File featuresFile = new File("C:/Users/usx29096/Desktop/malletTest/featureList"); //1 doc per line. 
//		//Simply put features like a document, split by white space. 
//		//Replace data white space with _,
//		//Affix the feature with type such as A_, V_
//		File instancesFile = new File("C:/Users/usx29096/Desktop/malletTest/MalletInstance");//tmp file for writing the instance file by loader
//		
//		loader.load(docFile, featuresFile, instancesFile);
//		
//		
//		InstanceList training = InstanceList.load (instancesFile);
//	
//	    int numTopics = 200;
//	
//	    DMRTopicModel lda = new DMRTopicModel (numTopics);
//	    
//		lda.setNumIterations(1000);
//		lda.setBurninPeriod(250);
//		lda.setOptimizeInterval(50);
//		
//		lda.setTopicDisplay(100, 10);
//		lda.addInstances(training);
//		lda.estimate();
//	
//		lda.writeParameters(new File("C:/Users/usx29096/Desktop/malletTest/dmr.parameters"));
//		lda.printState(new File("C:/Users/usx29096/Desktop/malletTest/dmr.state.gz"));
//		lda.printTopWords(new File("C:/Users/usx29096/Desktop/malletTest/topicWords"), 0, true);
//		lda.printDocumentTopics(new File("C:/Users/usx29096/Desktop/malletTest/docTopics"));
	
		
		//LDA
		
		// Begin by importing documents from text to feature sequences
       
		ArrayList<Pipe> pipeList = new ArrayList<Pipe>();

        // Pipes: lowercase, tokenize, remove stopwords, map to features
        pipeList.add( new CharSequenceLowercase() );
        pipeList.add( new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")));
        pipeList.add( new TokenSequence2FeatureSequence() );

        InstanceList instances = new InstanceList (new SerialPipes(pipeList));

        BufferedReader reader = new BufferedReader(new FileReader (args[0]));
        ArrayList<Instance> instanceBuffer = new ArrayList<Instance>();
        int lineNumber = 1;
        
        String wordsLine = null;
        while ((wordsLine = reader.readLine()) != null) {
			
			instanceBuffer.add(new Instance(wordsLine, null , String.valueOf(lineNumber), null));

			lineNumber++;
        }
        
        reader.close();
		instances.addThruPipe(instanceBuffer.iterator());


        // Create a model with 100 topics, alpha_t = 0.01, beta_w = 0.01
        //  Note that the first parameter is passed as the sum over topics, while
        //  the second is the parameter for a single dimension of the Dirichlet prior.
        int numTopics = 200;
        ParallelTopicModel model = new ParallelTopicModel(numTopics, 1.0, 0.01);

        model.addInstances(instances);

        // Use two parallel samplers, which each look at one half the corpus and combine
        //  statistics after every iteration.
        model.setNumThreads(1);

        // Run the model for 50 iterations and stop (this is for testing only, 
        //  for real applications, use 1000 to 2000 iterations)
        model.setNumIterations(1000);
        model.estimate();

        
        model.printTopWords(new File(args[1]), 0, true);
	
	}
}
